using System.Linq;
using Assets.Plugins.xNode.Scripts;
using UnityEngine;

namespace Assets.NodeUtilityAi.Nodes
{
    public class UtilityAverageNode : MiddleNode
    {
        [Node.InputAttribute(Node.ShowBackingValue.Never, Node.ConnectionType.Multiple)] public float UtilityInA;
        
        [Node.OutputAttribute] public float UtilityOut;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "UtilityOut")
            {
                var total = GetInputValues<float>(
                    "UtilityInA");
                float result = total.Sum() / total.Length;
                return result;


            }
            return null;
        }
    }
}