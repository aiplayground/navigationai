using Assets.Plugins.xNode.Scripts;

namespace Assets.NodeUtilityAi.Nodes
{
    public class UtilityMultiplyNode : MiddleNode
    {
        [Node.InputAttribute(Node.ShowBackingValue.Never, Node.ConnectionType.Multiple)] public float UtilityInA;
        [Node.InputAttribute(Node.ShowBackingValue.Never, Node.ConnectionType.Multiple)] public float UtilityInB;
        [Node.OutputAttribute] public float UtilityOut;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "UtilityOut")
            {
                var result =GetInputValue<float>(
                                "UtilityInA") * GetInputValue<float>("UtilityInB");
                return result;


            }
            return null;
        }
    }
}