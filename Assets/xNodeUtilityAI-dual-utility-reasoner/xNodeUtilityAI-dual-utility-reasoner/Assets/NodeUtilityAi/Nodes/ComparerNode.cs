using System;
using System.Linq;
using Assets.Plugins.xNode.Scripts;
using UnityEngine;

namespace Assets.NodeUtilityAi.Nodes
{
    public class ComparerNode : MiddleNode
    {
        public enum FilterType {
            Superior,
            Inferior,
            Equal
        }

        public enum FilterTypeB
        {
            Max, 
            Min,
            Average
        }
        
        
        [Input(ShowBackingValue.Never)] public float ValuesIn;
        public FilterTypeB EntryFilter;
        [Node.InputAttribute(Node.ShowBackingValue.Never)]
        public float ValueToCompare;
        public FilterType Filter;
        [Output] public float ValueOut;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "ValueOut")
            {
                float value = 0;
                var  result = GetInputValue<float>("ValueToCompare");
                ValueToCompare = result;
                NodePort valuesPort = GetInputPort("ValuesIn");
                if (valuesPort.IsConnected)
                {
                    float[] valuesIn = valuesPort.GetInputValues<float>();
                    switch (EntryFilter)
                    {
                        case FilterTypeB.Max :
                            value = Mathf.Max(valuesIn);
                            break;
                        case FilterTypeB.Min :
                            value = Mathf.Min(valuesIn);
                            break;
                        case FilterTypeB.Average :
                            value = valuesIn.Sum()/valuesIn.Length;
                            break;
                    }
                    switch (Filter)
                    {
                        case FilterType.Superior:

                            if (value > ValueToCompare)
                            {
                                return value;
                            }
                            else value = 0;
                            break;
                        case FilterType.Inferior:
                            
                            if (value < ValueToCompare)
                            {
                               return value;
                            }
                            else value = 0;
                            break;
                        case FilterType.Equal:

                            if (value == ValueToCompare)
                            {
                               return value;
                            }
                            else value = 0;
                            break;  
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                return value;
            }

            return null;
        }
    }
}