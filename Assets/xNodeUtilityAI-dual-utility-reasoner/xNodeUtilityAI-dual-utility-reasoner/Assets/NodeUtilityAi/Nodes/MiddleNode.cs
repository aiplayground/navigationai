﻿using Assets.Plugins.xNode.Scripts;

namespace Assets.NodeUtilityAi.Nodes {
    [NodeTint(120, 120, 255)]
    public abstract class MiddleNode : Node {}
}