using Assets.Plugins.xNode.Scripts;

namespace Assets.NodeUtilityAi.Nodes
{
    public class FloatToIntNode : MiddleNode
    {
        [Input(ShowBackingValue.Never, ConnectionType.Multiple)] public float UtilityIn;
        
        [Output] public int GetValueOut;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "GetValueOut")
            {
                return GetInputValue<float>(
                                "UtilityIn");
               
            }
            return null;
        }
    }
}