using Assets.Plugins.xNode.Scripts;
using UnityEngine;

namespace Assets.NodeUtilityAi.Nodes
{
    public class MultiplyNode : MiddleNode
    {
        [Input(ShowBackingValue.Never, ConnectionType.Multiple)] public float UtilityIn;
        [Input(ShowBackingValue.Never, ConnectionType.Multiple)] public int MultiplierIn;
        [Output] public int UtilityOut;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "UtilityOut")
            {
                var result =GetInputValue<int>(
                                "MultiplierIn") * GetInputValue<float>("UtilityIn");
                return (int) result;


            }
            return null;
        }
    }
}