﻿using Assets.NodeUtilityAi.Framework;

namespace Assets.NodeUtilityAi.Nodes {
    public abstract class SimpleActionNode : ActionNode {

        [Input(ShowBackingValue.Never)] public TaggedData Data;
        
    }

}