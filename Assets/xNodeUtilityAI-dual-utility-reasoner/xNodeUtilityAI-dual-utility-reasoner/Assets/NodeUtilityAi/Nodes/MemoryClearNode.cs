﻿using System.Collections.Generic;
using Assets.NodeUtilityAi.Framework;
using UnityEngine;

namespace Assets.NodeUtilityAi.Nodes {
    public class MemoryClearNode : SimpleActionNode {

        public override void Execute(AbstractAIComponent context, AIData aiData) {
            foreach (KeyValuePair<string,Object> keyValuePair in aiData) {
                context.ClearFromMemory(keyValuePair.Key);
            }
        }
    }
}