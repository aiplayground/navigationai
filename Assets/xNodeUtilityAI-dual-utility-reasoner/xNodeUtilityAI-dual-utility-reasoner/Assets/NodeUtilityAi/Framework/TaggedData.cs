﻿using System;
using Object = UnityEngine.Object;

namespace Assets.NodeUtilityAi.Framework {
    [Serializable]
    public class TaggedData {

        public string DataTag;
        public Object Data;

    }
}