﻿using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace Assets.Examples.CubeAI.Nodes {
    public class ReloadNode : SimpleActionNode {

        public override void Execute(AbstractAIComponent context, AIData aiData) {
            CubeAIComponent cubeAiComponent = (CubeAIComponent) context;
            cubeAiComponent.CubeEntity.Reload();
        }
        
    }
}