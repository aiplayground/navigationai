﻿using System;
using Assets.NodeUtilityAi;
using UnityEngine;

namespace Assets.Examples.CubeAI {
    [Serializable, CreateAssetMenu(fileName = "CubeAIBrain", menuName = "AI/CubeAIBrain")]
    public class CubeAIBrain : AbstractAIBrain {}
}