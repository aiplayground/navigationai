using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.ActionsNodes
{
    public class ExploreActionNode : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (!geneticAiComponent.geneticAIEntity.isActing)
            {
              
                geneticAiComponent.geneticAIEntity.Explore();
              
            }
        }
    }
}