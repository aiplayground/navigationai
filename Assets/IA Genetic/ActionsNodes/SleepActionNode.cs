using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.ActionsNodes
{
    public class SleepActionNode : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (!geneticAiComponent.geneticAIEntity.isActing)
            {
                
                geneticAiComponent.geneticAIEntity.ChoosenDestination = aiData.GetData<GameObject>().transform;
                geneticAiComponent.geneticAIEntity.GoSleep();
            }
        }
    }
}