using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.ActionsNodes
{
    public class UnlockActingActionNode : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
              
                geneticAiComponent.geneticAIEntity.isGoingEat = false;
                geneticAiComponent.geneticAIEntity.isGoingSleep = false;
                geneticAiComponent.geneticAIEntity.isGoingDrink = false;
                geneticAiComponent.geneticAIEntity.isActing = false;
                Debug.Log("OverrideActualAction");
            
            
        }
    }
}