using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.ActionsNodes
{
    public class ReproductActionNode : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (!geneticAiComponent.geneticAIEntity.isActing)
            {
              
               Debug.Log("Trying to Reproduct With " + aiData.GetData<GameObject>());
               geneticAiComponent.geneticAIEntity.Reproduce();
            }
        }
    }
}