using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;

namespace IA_Genetic.EntryNodes
{
    public class DistanceMaxEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            GeneticAIComponent myAIComponent = (GeneticAIComponent) context;
            return myAIComponent.geneticAIEntity.Range;
        }
    }
}