using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class NestCollectionNode : CollectionEntryNode
    {
        protected override List<Object> CollectionProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (geneticAiComponent.geneticAIEntity.Nidation.Count > 0)
            {
                List<GameObject> WaypointsNest = geneticAiComponent.geneticAIEntity.Nidation;
                return new List<Object>(WaypointsNest);
            }
            else return null;
        }
    }
}