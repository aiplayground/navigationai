using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class SelectBestPartnerEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            GameObject currentWP = GetData<GameObject>();
            if (currentWP != null)
            {
                var GenWp = currentWP.gameObject.GetComponent<GeneticAIEntity>();
                if (!GenWp.isSterile && !GenWp.isGestating && GenWp.GenderMale != geneticAiComponent.geneticAIEntity.GenderMale)
                {
                    var result = GenWp.Maturity * GenWp.CanReproduce;
                    return (int) result;
                }
                else
                {
                    return 0;
                }
            }
            else return 0;
        }
    }
}