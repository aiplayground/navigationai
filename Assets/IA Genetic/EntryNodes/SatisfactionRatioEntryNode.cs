using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;

namespace IA_Genetic.EntryNodes
{
    public class SatisfactionRatioEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            return (int) geneticAiComponent.geneticAIEntity.AverageNeedsRatio;
        }
    }
}