using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class WaterSourceCollectionNode: CollectionEntryNode
    {
        protected override List<Object> CollectionProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (geneticAiComponent.geneticAIEntity.WaterSources.Count > 0)
            {
                List<GameObject> WaypointsFood = geneticAiComponent.geneticAIEntity.WaterSources;
                return new List<Object>(WaypointsFood);
            }
            else return null;

        }
    }
}