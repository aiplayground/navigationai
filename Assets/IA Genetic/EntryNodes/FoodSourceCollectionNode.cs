using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class FoodSourceCollectionNode : CollectionEntryNode
    {
        protected override List<Object> CollectionProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (geneticAiComponent.geneticAIEntity.FoodSources.Count > 0)
            {
                List<GameObject> WaypointsFood = geneticAiComponent.geneticAIEntity.FoodSources;
                return new List<Object>(WaypointsFood);
            }
            else return null;

        }
    }
}