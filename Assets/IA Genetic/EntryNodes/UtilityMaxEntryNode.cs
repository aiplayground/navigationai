using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;

namespace IA_Genetic.EntryNodes
{
    public class UtilityMaxEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            return 1;
        }
    }
}