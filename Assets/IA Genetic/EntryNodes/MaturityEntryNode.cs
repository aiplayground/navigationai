using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;

namespace IA_Genetic.EntryNodes
{
    public class MaturityEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            var result = geneticAiComponent.geneticAIEntity.Maturity * geneticAiComponent.geneticAIEntity.CanReproduce;
            return (int) result;
        }
    }
}