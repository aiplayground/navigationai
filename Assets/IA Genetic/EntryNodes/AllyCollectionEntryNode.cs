using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class AllyCollectionEntryNode : CollectionEntryNode
    {
        protected override List<Object> CollectionProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            if (geneticAiComponent.geneticAIEntity.AllyKnoww.Count > 0)
            {
                List<GameObject> Allies = geneticAiComponent.geneticAIEntity.AllyKnoww;
                return new List<Object>(Allies);
            }
            else return null;

        }
    }
}