using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class SelectWayEntryNode : SimpleEntryNode
    {
    
        protected override int ValueProvider(AbstractAIComponent context)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            GameObject currentWP = GetData<GameObject>();
            if (currentWP != null)
            {
                return (int) Vector3.Distance(currentWP.transform.position,
                    geneticAiComponent.geneticAIEntity.transform.position);
            }
            else return 0;
        }
    }
}