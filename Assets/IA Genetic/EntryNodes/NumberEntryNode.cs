using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;

namespace IA_Genetic.EntryNodes
{
    public class NumberEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            return (int) 0.7f;
        }
    }
}