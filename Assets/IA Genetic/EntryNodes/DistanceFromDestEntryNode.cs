using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA;
using IA_Genetic.Management;
using UnityEngine;

namespace IA_Genetic.EntryNodes
{
    public class DistanceFromDestEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            GeneticAIComponent myAIComponent = (GeneticAIComponent) context;
            return (int) Vector3.Distance(myAIComponent.geneticAIEntity.transform.position,
                myAIComponent.geneticAIEntity.PointToGo.position);
        }
    }
}