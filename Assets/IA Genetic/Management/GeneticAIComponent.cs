using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;

namespace IA_Genetic.Management
{
    public class GeneticAIComponent : AbstractAIComponent
    {
        public GeneticAIEntity geneticAIEntity;
        private void Awake()
        {
            geneticAIEntity = GetComponent<GeneticAIEntity>();
        }
        private void Start()
        {
            InvokeRepeating("ThinkAndAct", 0, 0.2f);
        }

        private void ThinkAndAct()
        {
            AIOption option = ChooseOption();
            if (option == null) return;
            option.ExecuteActions(this);
        }
    }
}