using UnityEngine;

namespace IA_Genetic.Management
{
    public class AISensor : MonoBehaviour
    {
        public GeneticAIEntity geneticAiEntity;
        // Start is called before the first frame update
        void Awake()
        {
            geneticAiEntity = GetComponentInParent<GeneticAIEntity>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Food") && geneticAiEntity.FoodSources.Contains(other.gameObject) == false)
            {
                if (geneticAiEntity.FoodSources.Count > 3)
                {
                    geneticAiEntity.FoodSources.Clear();
                }
                geneticAiEntity.FoodSources.Add(other.gameObject);
            }
            if (other.CompareTag("Sleep") && geneticAiEntity.Nidation.Contains(other.gameObject) == false)
            {
                if (geneticAiEntity.Nidation.Count > 3)
                {
                    geneticAiEntity.Nidation.Clear();
                }
                geneticAiEntity.Nidation.Add(other.gameObject);
            }
            if (other.CompareTag("Water") && geneticAiEntity.WaterSources.Contains(other.gameObject) == false)
            {
                if (geneticAiEntity.WaterSources.Count > 3)
                {
                    geneticAiEntity.WaterSources.Clear();
                }
                geneticAiEntity.WaterSources.Add(other.gameObject);
            }
            if (other.CompareTag("Ennemy"))
            {
                if (geneticAiEntity.EnnemiesKnowPosition.Contains(other.gameObject) == false)
                {
                    geneticAiEntity.EnnemiesKnowPosition.Add(other.gameObject);
                }
                
            }
            if (other.CompareTag("Player") )
            {
                if (geneticAiEntity.AllyKnoww.Contains(other.gameObject) == false)
                {
                    if (geneticAiEntity.AllyKnoww.Count > 3)
                    {
                        geneticAiEntity.AllyKnoww.Clear();
                    }
                    geneticAiEntity.AllyKnoww.Add(other.gameObject);
                }
                
            }
            
        }

        private void FixedUpdate()
        {
            geneticAiEntity.EnnemiesKnowPosition.RemoveAll(GameObject => GameObject == null);
            geneticAiEntity.AllyKnoww.RemoveAll(GameObject => GameObject == null);
        }

        private void OnTriggerExit(Collider other)
        {
           
        }
    }
}