using System;
using Assets.NodeUtilityAi;
using UnityEngine;

namespace IA_Genetic.Management
{
    public class GeneticAIBrain : AbstractAIBrain
    {
        [Serializable, CreateAssetMenu(fileName = "GeneticBrain", menuName = "AI/Brains/Genetic")]
        public class MyAIBrain : AbstractAIBrain
        {
            // Start is called before the first frame update
            void Start()
            {
        
            }

            // Update is called once per frame
            void Update()
            {
        
            }
        }
    
    }
}