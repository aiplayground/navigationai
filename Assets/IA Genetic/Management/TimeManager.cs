using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IA_Genetic.Management
{
    public class TimeManager : MonoBehaviour
    {
        public List<GeneticAIEntity> AIEntity =new List<GeneticAIEntity>();
        public int Generation;
        public int NumberEntity;
        public int NumberDead;
        public Text Nbr;
        public Text Gene;
        public Text Dead;
        private void Start()
        {
            GeneticAIEntity[] genetic = FindObjectsOfType<GeneticAIEntity>();
            foreach (var gen in genetic)
            {
                AIEntity.Add(gen);
                NumberEntity++;
                
            }

            GeneticAIEntity.Birth += IncreaseNumber;
            GeneticAIEntity.Death += DecreaseNumber;
        }

        private void FixedUpdate()
        {
            Gene.text = Generation.ToString();
            Nbr.text = NumberEntity.ToString();
            Dead.text = NumberDead.ToString();
        }

        public void Generations(int gen)
        {
            if (gen > Generation) Generation = gen;
        }
        void IncreaseNumber()
        {
            NumberEntity++;
        }
        void DecreaseNumber()
        {
            NumberDead++;
        }
    }
}