using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;


namespace IA_Genetic.Management
{
    public class GeneticAIEntity : MonoBehaviour
    {
        public List<GameObject> WaterSources = new List<GameObject>();
        public List<GameObject> FoodSources = new List<GameObject>();
        public List<GameObject> EnnemiesKnowPosition = new List<GameObject>();
        public List<GameObject> AllyKnoww = new List<GameObject>();
        public List<GameObject> Nidation = new List<GameObject>();
        public Transform PointToGo;
        public NavMeshAgent agent;

        public GameObject AIPrefab;

        
        public Transform ChoosenDestination;
        public Transform ExploreDestination;
        public bool isActing;
        public bool isGoingDrink;
        public bool isGoingEat;
        public bool isGoingSleep;
        public bool isExploring;
       
        [Header("StatisticEntity")] 
        [UnityEngine.Range (0f, 100f)] public float FoodNeed;
        [UnityEngine.Range (0f, 100f)] public float WaterNeed;
        [UnityEngine.Range (0f, 100f)] public float Energy;
        [UnityEngine.Range (0f, 100f)] public float Health;

        public bool GenderMale;
        public bool GenderFemale;
        public bool isGestating;
        public int GestationDuration;
        public float gestateTimer;
        public bool isSterile;
        public float Age;
        public float Maturity;
        public float CanReproduce;
        public float lifeDuration;
        public float MaxLifeDuration;
        public AnimationCurve MaturityCurve;
        public float MaxCuriosity;
        public float Curiosity;
        public AnimationCurve curiosityCurve;

        public float AverageNeedsRatio;
        public float StatMax;
        public int Range;

        public float VieillissementSpeed;
        public float FoodRatio;
        public float WaterRatio;
        public float SleepRatio;
        [Header("Sensing")] 
        public AISensor sensor;

        public static event System.Action Birth;
        public static event System.Action Death;
        public TimeManager manager;
        public int Generation;
        private void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            sensor = GetComponentInChildren<AISensor>();
            manager = FindObjectOfType<TimeManager>();
            Health = 100f;
            Age = 0;
            CanReproduce = 0;
            lifeDuration = Random.Range(2f, MaxLifeDuration);
            GenderGenerate();
            manager.Generations(Generation);
        }

        void GenderGenerate()
        {
            var Sexe = Random.Range(0f, 2f);
            if (Sexe <= 1f)
            {
                GenderMale = true;
                GenderFemale = false;
            }

            if (Sexe >1f)
            {
                GenderFemale = true;
                GenderMale = false;
            }
        }

        public void MoveTo(Transform dest)
        {
            Debug.Log("GoTo");
            if(!isActing &&agent.isOnNavMesh) agent.SetDestination(dest.position);
        }
        public void MoveToVect(Vector3 dest)
        {
            Debug.Log("GoTo");
            if(!isActing && agent.isOnNavMesh) agent.SetDestination(dest);
        }
        public void GoEat()
        {
            Debug.Log("Eat");
           
            MoveTo(ChoosenDestination);
            isGoingEat = true;
            isActing = true;
            
        }

        public void GoDring()
        {
            Debug.Log("Drink");
           
            MoveTo(ChoosenDestination);
            isGoingDrink = true;
            isActing = true;
           
        }
        
        public void GoSleep()
        {
            Debug.Log("Sleep");
           
            MoveTo(ChoosenDestination);
            isGoingSleep = true;
            isActing = true;
            
        }

        public void Reproduce()
        {
            if (GenderFemale)
            {
                isGestating = true;
            }
        }

        void Gestation()
        {
            if (isGestating)
            {
                gestateTimer += Time.deltaTime;
                if (gestateTimer > GestationDuration)
                {
                    var Child = Instantiate(AIPrefab);
                    var ChildBrain = Child.GetComponent<GeneticAIEntity>();
                    Debug.Log(ChildBrain.agent.isOnNavMesh);
                    
                    Vector3 location = new Vector3(transform.position.x+ UnityEngine.Random.Range(-1, 1), 0, transform.position.z+ UnityEngine.Random.Range(-1, 1));
                    ChildBrain.agent.Warp(location);
                    ChildBrain.Generation = Generation + 1;
                    ChildBrain.WaterSources = WaterSources;
                    ChildBrain.FoodSources = FoodSources;
                    ChildBrain.AllyKnoww = AllyKnoww;
                    ChildBrain.Nidation = Nidation;
                    ChildBrain.Age = 0;
                    ChildBrain.isGestating = false;
                    ChildBrain.gestateTimer = 0f;
                    ChildBrain.GestationDuration = GestationDuration;
                    gestateTimer = 0f;
                    isSterile = RandomBool(50);
                    Birth.Invoke();
                    isGestating = false;
                }
            }
        }
        void CheckArrivedAndAct()
        {
            if (ChoosenDestination != null)
            {
                if (Vector3.Distance(transform.position, ChoosenDestination.position) < 2)
                {
                    if (isGoingEat)
                    {
                        FoodNeed = 0;
                        isGoingEat = false;
                    }

                    if (isGoingDrink)
                    {
                        WaterNeed = 0;
                        isGoingDrink = false;
                    }

                    if (isGoingSleep)
                    {
                        Energy = 0;
                        isGoingSleep = false;
                    }
                    if (isExploring)
                    {
                       isExploring = false;
                    }
                    isActing = false;
                }
            }
            
        }

        public void Explore()
        {
            Debug.Log("Exploring");
            float x = Random.Range(0, 250);
            float z = Random.Range(0, 250);
            Vector3 pos = new Vector3(x, 1, z);
            MoveToVect(pos);
            isExploring = true;
            isActing = true;


        }
        private void FixedUpdate()
        {
            AverageNeedsRatio = (Energy+FoodNeed+WaterNeed+Health)/4;
            IncreaseNeedsInTime();
            CheckArrivedAndAct();
            GetFecondity();
            GetCuriosity();
            Gestation();
        }

        void IncreaseNeedsInTime()
        {
            
            FoodNeed += Time.deltaTime * FoodRatio;
            WaterNeed += Time.deltaTime * WaterRatio;
            Energy += Time.deltaTime * SleepRatio;
            Age += Time.deltaTime * VieillissementSpeed;
            FoodNeed = Mathf.Clamp(FoodNeed, 0, 100);
            WaterNeed = Mathf.Clamp(WaterNeed, 0, 100);
            Energy = Mathf.Clamp(Energy, 0, 100);
            if (FoodNeed > 99f|| WaterNeed > 99f|| Energy > 99f)
            {
                Health -= 0.1f * Time.deltaTime;
            }

            if (Maturity > 30f && !isGestating && !isSterile) CanReproduce = 1;
            if (Age > lifeDuration || Health <= 0)
            {
                Debug.Log("AI Died");
                Death.Invoke();
                gameObject.SetActive(false);
                //Destroy(gameObject);
            }
        }



        void GetFecondity()
        {
            var result = MaturityCurve.Evaluate(Age / lifeDuration);
            Maturity = result*100;
        }

        void GetCuriosity()
        {
            var result = curiosityCurve.Evaluate(AverageNeedsRatio / MaxCuriosity);
            Curiosity = result * MaxCuriosity;
        }


        static bool RandomBool(float purcentage)
        {
            float Rand = 0;
            Rand = Random.Range(0, 100);
            return (Rand > purcentage);

        }
    }
}