using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;

namespace IA.EntryNodes
{
    public class MinusXAxisEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            MyAIComponent myAIComponent = (MyAIComponent) context;
            return (int)myAIComponent.NeuralEntity.minusXAxisDistance;
        }
    }
}