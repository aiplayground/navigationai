using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;

namespace IA.EntryNodes
{
    public class AimAtPointEntryNode  : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            MyAIComponent myAIComponent = (MyAIComponent) context;
            return (int) context.Options[0].Utility;
        }
        
    }
}