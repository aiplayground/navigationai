using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace IA.EntryNodes
{
    public class WayPointCollectionEntryNode : CollectionEntryNode
    {
        protected override List<Object> CollectionProvider(AbstractAIComponent context)
        {
            MyAIComponent myAIComponent = (MyAIComponent) context;
            List<Transform> Waypoints =  myAIComponent.NeuralEntity.wpInRange;
            return new List<Object>(Waypoints);
        }
    }
}