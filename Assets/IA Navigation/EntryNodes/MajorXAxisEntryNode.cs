using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;

namespace IA.EntryNodes
{
    public class MajorXAxisEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            MyAIComponent myAIComponent = (MyAIComponent) context;
            return (int)myAIComponent.NeuralEntity.majorXAxisDistance;
        }
    }
}