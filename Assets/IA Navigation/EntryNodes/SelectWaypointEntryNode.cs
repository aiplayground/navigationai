﻿using System.Collections;
using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using IA;
using UnityEngine;

public class SelectWaypointEntryNode : SimpleEntryNode
{
    
    protected override int ValueProvider(AbstractAIComponent context)
    {
        MyAIComponent myAIComponent = (MyAIComponent) context;
        Transform currentWP = GetData<Transform>();
        return (int) Vector3.Distance(currentWP.position, myAIComponent.NeuralEntity.PointToGo.position);
    }
}
