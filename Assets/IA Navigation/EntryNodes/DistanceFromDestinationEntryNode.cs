using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace IA.EntryNodes
{
    public class DistanceFromDestinationEntryNode : SimpleEntryNode
    {
        protected override int ValueProvider(AbstractAIComponent context)
        {
            MyAIComponent myAIComponent = (MyAIComponent) context;
            return (int) Vector3.Distance(myAIComponent.NeuralEntity.transform.position,
                myAIComponent.NeuralEntity.PointToGo.position);
        }
    }
}