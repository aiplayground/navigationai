﻿using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using UnityEngine.Serialization;

namespace IA
{
    public class MyAIComponent : AbstractAIComponent
    {
        public AINeuralEntity NeuralEntity;
        private void Awake()
        {
            NeuralEntity = GetComponent<AINeuralEntity>();
        }
        private void Start()
        {
            InvokeRepeating("ThinkAndAct", 0, 0.04f);
        }

        private void ThinkAndAct()
        {
           AIOption option = ChooseOption();
           if (option == null) return;
           option.ExecuteActions(this);
        }
        
       
    }
}




