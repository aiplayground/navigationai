using System.Collections.Generic;

using UnityEngine;

namespace IA
{
    public class AINeuralEntity : MonoBehaviour
    {
        [Header("Sensing")]
        public float zAxisDistance;
        public float minusXAxisDistance;
        public float majorXAxisDistance;
        public float ZminusXAxisDistance;
        public float ZmajorXAxisDistance;

        public float DetectDistance;

        [Header("Controlling")]
        public CharacterController controller;
        Vector3 moveDirection = Vector3.zero;
        public Transform PointToGo;
        public int hasPointToGo;
        public bool FollowGoToPoint;
        public float MoveZAxis;
        public float Rot;
        public float RotSensitivity = 50f;
        public float Speed = 0.05f;
        public bool isLeft;
        public bool isRight;
        [Header("Memorizing")]
        public List<Transform> wps = new List<Transform>();
        public List<Transform> wpInRange = new List<Transform>();
        public GameObject WPprefab;
        public float distanceToNearWP = 10;
        
        private void Start()
        {
            controller = GetComponent<CharacterController>();
        }

        void GetDistancesFromAxis()
        {
            RaycastHit hitZ;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hitZ,
                DetectDistance, layerMask: 9))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hitZ.distance,
                    Color.green);
                zAxisDistance = hitZ.distance;
            }
            else zAxisDistance = DetectDistance;

            RaycastHit hitMinusX;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hitMinusX,
                DetectDistance,layerMask: 9))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.left) * hitMinusX.distance,
                    Color.yellow);
                minusXAxisDistance = hitMinusX.distance;
            }
            else minusXAxisDistance = DetectDistance;

            RaycastHit hitMajorX;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hitMajorX,
                DetectDistance,layerMask: 9))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * hitMajorX.distance,
                    Color.yellow);
                majorXAxisDistance = hitMajorX.distance;
            }
            else majorXAxisDistance = DetectDistance;

            RaycastHit hitXminusZ;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward + Vector3.left),
                out hitXminusZ, DetectDistance,layerMask: 9))
            {
                Debug.DrawRay(transform.position,
                    transform.TransformDirection(Vector3.forward + Vector3.left) * hitXminusZ.distance, Color.red);
                ZminusXAxisDistance = hitXminusZ.distance;
            }
            else ZminusXAxisDistance = DetectDistance;

            RaycastHit hitXmajorZ;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward + Vector3.right),
                out hitXmajorZ, DetectDistance,layerMask: 9))
            {
                Debug.DrawRay(transform.position,
                    transform.TransformDirection(Vector3.forward + Vector3.right) * hitXmajorZ.distance, Color.red);
                ZmajorXAxisDistance = hitXmajorZ.distance;
            }
            else ZmajorXAxisDistance = DetectDistance;
        }

        private void Update()
        {
            
            GetDistancesFromAxis();
           CharacterControl();
           AimAtChosenPoint();
           GetNearestWP();
        }

        public bool CheckWayPointDataInRange()
        {
            bool state = wpInRange.Count > 0;
            return state;
        }
       
        public void AddWayPointData()
        {
            if (CheckWayPointDataInRange() == false)
            {
                var wp = Instantiate(WPprefab, transform.position, Quaternion.identity);
                wps.Add(wp.transform);
            }
           
        }
        public void CharacterControl()
        {
           
            if (controller.isGrounded){
             transform.Rotate(new Vector3(0, Rot, 0), RotSensitivity*Time.deltaTime, Space.World);
             Vector3 dir = transform.TransformDirection(Vector3.forward);
             moveDirection = dir.normalized*MoveZAxis*Speed;
             if(FollowGoToPoint) transform.LookAt(PointToGo);
            
            }
            moveDirection.y -= 10f*Time.deltaTime;
            controller.Move(moveDirection);
           

        }

        public void AimAtChosenPoint()
        {
            if (controller.isGrounded)
            {
                Vector3 AIDirection = transform.TransformDirection(Vector3.forward);
                Vector3 WantedDirection = transform.position - PointToGo.position;
                float AngleBetDir = Vector3.Angle(AIDirection, WantedDirection);
               
               
          }
       
        }

        public void GetNearestWP()
        {
            if (wpInRange.Count > 0)
            {
                wpInRange.Sort(delegate(Transform A, Transform B)
                {
                    return Vector3.Distance(A.position, transform.position)
                        .CompareTo(Vector3.Distance(B.position, transform.position));  });
                distanceToNearWP = Vector3.Distance(wpInRange[0].position, transform.position);
            }
           
        }

        
        public void MoveTo(Transform getData)
        {
            ///
            Debug.Log(getData);
        }
    }
}