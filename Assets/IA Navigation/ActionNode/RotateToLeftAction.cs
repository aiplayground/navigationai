using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace IA.ActionNode
{
    public class RotateToLeftAction : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            MyAIComponent myAiComponent= (MyAIComponent) context;
            if (!myAiComponent.NeuralEntity.isRight)
                {
                    myAiComponent.NeuralEntity.isLeft = true;
                    myAiComponent.NeuralEntity.Rot=-1f;
                }
            myAiComponent.NeuralEntity.AddWayPointData();
        }
    }
}