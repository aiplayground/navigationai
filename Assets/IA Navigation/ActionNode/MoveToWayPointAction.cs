using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using IA_Genetic.Management;
using UnityEngine;

namespace IA.ActionNode
{
    public class MoveToWayPointAction : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            GeneticAIComponent geneticAiComponent = (GeneticAIComponent) context;
            geneticAiComponent.geneticAIEntity.MoveTo(aiData.GetData<Transform>());
        }
    }
}