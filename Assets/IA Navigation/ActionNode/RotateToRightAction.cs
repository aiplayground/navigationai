using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace IA.ActionNode
{
    public class RotateToRightAction : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            MyAIComponent myAiComponent= (MyAIComponent) context;
            
            if (!myAiComponent.NeuralEntity.isLeft)
            {
                myAiComponent.NeuralEntity.isRight = true;
                myAiComponent.NeuralEntity.Rot=1f;
            }
            myAiComponent.NeuralEntity.AddWayPointData();
        }
    }
}