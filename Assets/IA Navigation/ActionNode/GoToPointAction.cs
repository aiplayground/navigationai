using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace IA.ActionNode
{
    public class GoToPointAction : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            MyAIComponent myAiComponent= (MyAIComponent) context;
            myAiComponent.NeuralEntity.FollowGoToPoint = true;
            myAiComponent.NeuralEntity.MoveZAxis=(context.Options[0].Utility);
        }
    }
}