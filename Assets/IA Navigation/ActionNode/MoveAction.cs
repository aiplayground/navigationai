using System.Collections.Generic;
using Assets.NodeUtilityAi;
using Assets.NodeUtilityAi.Framework;
using Assets.NodeUtilityAi.Nodes;
using UnityEngine;

namespace IA.ActionNode
{
    public class MoveAction : SimpleActionNode
    {
        public override void Execute(AbstractAIComponent context, AIData aiData)
        {
            Debug.Log("Act");
            MyAIComponent myAiComponent= (MyAIComponent) context;
            myAiComponent.NeuralEntity.FollowGoToPoint = false;
            myAiComponent.NeuralEntity.isLeft = false;
            myAiComponent.NeuralEntity.isRight = false;
            myAiComponent.NeuralEntity.Rot=0f;
            myAiComponent.NeuralEntity.MoveZAxis=(context.Options[0].Utility);
        }
    }
}