﻿using System.Collections;
using System.Collections.Generic;
using IA;
using UnityEngine;

public class NeuralSensor : MonoBehaviour
{
    private AINeuralEntity neuralEntity;
    // Start is called before the first frame update
    void Start()
    {
        neuralEntity = GetComponentInParent<AINeuralEntity>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Respawn"))
        {
            neuralEntity.wpInRange.Add(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        neuralEntity.wpInRange.Remove(other.transform);
    }
}
