﻿
using System;
using Assets.NodeUtilityAi;
using UnityEngine;

namespace IA
{
    [Serializable, CreateAssetMenu(fileName = "NeuralBrain", menuName = "AI/Brains")]
    public class MyAIBrain : AbstractAIBrain
    {
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
